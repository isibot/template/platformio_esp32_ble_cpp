#include "NimBLEDevice.h"
#include <cstdio>
#include <string>

#include "ble_handler.h"

namespace isibot
{

BleHandler::BleHandler()
{
	NimBLEDevice::init("BLE test serial");
	NimBLEServer &server = *NimBLEDevice::createServer();

	NimBLEService &service  = *server.createService(uuid::service);
	{
		auto &charac = *service.createCharacteristic(uuid::val);
		charac.setCallbacks(&_valUpdater);
		charac.setValue(0);
	}
	service.start();

	auto &advertising = *NimBLEDevice::getAdvertising();
	advertising.addServiceUUID(uuid::advertising);
	advertising.setScanResponse(true);
	NimBLEDevice::startAdvertising();
}

}  // namespace isibot
