#pragma once

#include <NimBLECharacteristic.h>
#include <NimBLEUUID.h>
#include <cstdio>
#include <string>

namespace isibot
{

namespace uuid  // clang-format off
{
	NimBLEUUID const advertising{0xaabbccdd, 0x4242, 0x1337, 0x151b07};
	NimBLEUUID const service    {0, 0x1111, 0x1337, 0x151b07};
	NimBLEUUID const val        {1, 0x1111, 0x1337, 0x151b07};
}  // clang-format on

template <class T>
struct AttributeUpdater : public NimBLECharacteristicCallbacks
{
	void onWrite(NimBLECharacteristic *charac)
	{
		T val = charac->getValue<T>();
		printf("onWrite, value: %s (uuid: %s)\n", std::to_string(val).c_str(),
		       charac->getUUID().toString().c_str());
	}

	void onRead(NimBLECharacteristic *charac)
	{
		T val = charac->getValue<T>();
		printf("onRead, value: %s (uuid: %s)\n", std::to_string(val).c_str(),
		       charac->getUUID().toString().c_str());
	}
};

struct BleHandler
{
	BleHandler();

private:
	AttributeUpdater<int> _valUpdater;
};

}  // namespace isibot
