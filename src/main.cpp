#include <cstdio>

#include "ble_handler.h"

extern "C" void app_main() {
    isibot::BleHandler bh;
    std::puts("started");

    while(true) {
        vTaskDelay(500 / portTICK_PERIOD_MS);
    }
}

